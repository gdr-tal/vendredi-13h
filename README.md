Ce projet gitlab héberge le groupe de lecture du GDR TAL
--------------------------------------------------------

Le mode de fonctionnement est le suivant :
1. Chacun peut proposer des articles à la lecture
2. On peut ensuite voter pour les articles à lire
3. À partir des votes et en assurant un équilibre thématique, deux ou trois articles sont sélectionnés
4. Les participants doivent avoir lu les articles avant la réunion
5. Lors de la réunion un "parain" présente rapidement (5 minutes) le contenu de l'article, puis tout le monde discute
6. En fin de réunion, on sélectionne les articles et parains pour la prochaine itération
7. Les issues associées à un article déjà lu sont fermées

**Comment proposer un article ?**
- Il faut se connecter à gitlab et créer une "issue". Cette dernière devra contenir les titre, auteurs, résumé et un lien vers le pdf. Les issues ne doivent pas être utilisées pour autre chose.

**Comment voter pour un article ?**
- Cliquez sur le pouce (👍) associé à l'issue

**Prochaine session**
- A la rentrée en septembre !! Bonnes vacances à tous !!

**Précédentes sessions groupes de lecture (2020) :**
- 24 janvier 2020
    - Towards Zero-shot Language Modeling - https://www.aclweb.org/anthology/D19-1288.pdf
    - Attending to Future Tokens for Bidirectional Sequence Generation - https://www.aclweb.org/anthology/D19-1001/
- 7 février 2020
    -  Reformer: The Efficient Transformer - https://arxiv.org/abs/2001.04451
    -  Knowledge Enhanced Contextual Word Representations - https://www.aclweb.org/anthology/D19-1005.pdf
- 21 février 2020
    - RoBERTa: A Robustly Optimized BERT Pretraining Approach - https://arxiv.org/abs/1907.11692v1
    - GENERATIVE PRE-TRAINING FOR SPEECH WITH AUTOREGRESSIVE PREDICTIVE CODING - https://arxiv.org/pdf/1910.12607.pdf
- 6 mars 2020
    -  Encoding word order in complex embeddings - https://gitlab.com/gdr-tal/vendredi-13h/issues/22
    -  On the Cross-lingual Transferability of Monolingual Representations - https://gitlab.com/gdr-tal/vendredi-13h/issues/27
- 27 mars 2020
    - ALBERT: A Lite BERT for Self-supervised Learning of Language Representations
    - The Curious Case of Neural Text Degeneration
- 24 avril 2020
    - Parameter-Efficient Transfer Learning for NLP - https://arxiv.org/abs/1902.00751
    - Blank Language Models - https://arxiv.org/abs/2002.03079
- 15 mai 2020
    - Information-Theoretic Probing with Minimum Description Length - https://arxiv.org/abs/2003.12298
    - A Structural Probe for Finding Syntax in Word Representations - https://www.aclweb.org/anthology/N19-1419/
- 29 mai 2020
    - Unstoppable Rise of Computational Linguistics in Deep Learning - https://arxiv.org/abs/2005.06420
    - Compressive Transformers for Long-range Distance Modeling - https://openreview.net/pdf?id=SylKikSYDH
- 12 juin 2020
    -  Language Models are Few-Shot Learners - https://arxiv.org/abs/2005.14165	
    -  How Can We Accelerate Progress Towards Human-like Linguistic Generalization? - https://arxiv.org/abs/2005.00955
- 26 juin 2020
    -  Sequence Modeling with Unconstrained Generation Order - https://arxiv.org/abs/1911.00176
    -  An Unsupervised Autoregressive Model for Speech Representation Learning https://arxiv.org/abs/1904.03240
- 10 juillet 2020 : discussion autour plusieurs papiers présentés dans la semaine à ACL "virtuel"
    - https://arxiv.org/pdf/1910.00553.pdf
    - https://www.aclweb.org/anthology/2020.acl-main.615.pdf
    - https://www.aclweb.org/anthology/2020.acl-main.198.pdf
    - https://www.aclweb.org/anthology/2020.acl-main.275.pdf
    - https://www.aclweb.org/anthology/2020.acl-main.688.pdf
    - https://arxiv.org/pdf/2005.00956v1.pdf
